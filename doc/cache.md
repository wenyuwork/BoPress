# Cache #

> BoPress Cache 使用第三方 `werkzeug.contrib.cache` 的Cache库。 在此基础上增加明确的分组缓存。

    from bopress.cache import Cache
    
	Cache.default().set(['a','b'], object, 300)
	Cache.default().set(['a','c'], object, 500)
	Cache.default().set(['a','d'], object, None)

删除`a`分组下的缓存

	Cache.default().delete(['a'])

其它cache方法请参阅 `bopress.cache` 模块。

> BoPress 的Session也用此Cache方式实现。

至于采用哪种缓存方式，可以在全局配置中指定。
    

 