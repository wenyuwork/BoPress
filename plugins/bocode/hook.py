# -*- coding: utf-8 -*-


from bopress.hook import add_menu_page, add_submenu_page

__author__ = 'yezang'


def add_code_generation_menu_page(page_title, menu_title, menu_slug='', position=10):
    """
    添加代码生成菜单页面
    :param page_title:
    :param menu_title:
    :param menu_slug:
    :param position:
    """
    add_menu_page(page_title, menu_title, menu_slug, ["r_list_users"], "bocode/tpl/code-generation.html", "", position)


def add_code_generation_submenu_page(parent_slug, page_title, menu_title, menu_slug=''):
    """
    添加代码生成子菜单页面
    :param parent_slug:
    :param page_title:
    :param menu_title:
    :param menu_slug:
    """
    add_submenu_page(parent_slug, page_title, menu_title, menu_slug, ["r_list_users"], "bocode/tpl/code-generation.html",
                     "")
